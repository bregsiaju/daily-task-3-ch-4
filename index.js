// Imports
const express = require('express');
const { PORT = 8000 } = process.env;
const app = express();
const datas = require('./src/data');

// import data handler / filter
const { data1Handler, data2Handler, data3Handler, data4Handler, data5Handler } = require('./src/filtering');
const findData = require('./src/findData');

// set view engine to ejs
app.set('view engine', 'ejs');
// Static Files
app.use(express.static(__dirname + '/public'));

// display homepage
app.get('/', (req, res) => {
  res.render('index', {
    title: 'Homepage'
  });
});

// display about page
app.get('/about', (req, res) => {
  res.render('about', {
    title: 'About'
  });
});

// get and filter data from submitted form (search)
app.get('/data', (req, res) => {
  const foundData = findData(req.query.key, req.query.category);
  res.render('biodata', {
    dataOutput: foundData,
    note: `Searched keyword = <b>${req.query.category} is ${req.query.key}</b>`,
    title: 'User List'
  });
});

// display all data or filtered data on the table view (ejs)
app.get('/filter/:filter', (req, res) => {
  const filtered = req.params.filter;
  let dataOutput, note;

  switch (filtered) {
    case 'alldata':
      dataOutput = datas;
      note = 'Tampilkan semua data';
      break;
    case 'data1':
      dataOutput = data1Handler();
      note = 'Age dibawah 30 tahun dan favorit buah pisang';
      break;
    case 'data2':
      dataOutput = data2Handler();
      note = 'Gender female atau company FSW4 dan age diatas 30 tahun';
      break;
    case 'data3':
      dataOutput = data3Handler();
      note = 'Warna mata biru dan age di antara 35 sampai dengan 40, dan favorit buah apel';
      break;
    case 'data4':
      dataOutput = data4Handler();
      note = 'Company Pelangi atau Intel, dan warna mata hijau';
      break;
    case 'data5':
      dataOutput = data5Handler();
      note = 'Registered di bawah tahun 2016 dan masih active (true)';
      break;
  }

  res.render('biodata', {
    dataOutput,
    note,
    title: 'User List'
  });
});

// set tampilan default apa pun url-nya
app.use("/", (req, res) => {
  res.status(404);
  res.render('404');
});

// run server
app.listen(PORT, () => {
  console.log(`Server is listening on http://localhost:${PORT}`);
});