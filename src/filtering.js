// import data
const datas = require('./data');

const errorHandler = () => {
  const err = {
    message: "data tidak ada / tidak ditemukan"
  };
  return err;
};

const data1Handler = () => {
  const filteredData = [];
  for (let i = 0; i < datas.length; i++) {
    // age dibawah 30 tahun dan favorit bua pisang
    if (datas[i].age < 30 && datas[i].favoriteFruit === "banana") {
      filteredData.push(datas[i]);
    }
  }
  return (filteredData.length < 1) ? errorHandler() : filteredData;
};

const data2Handler = () => {
  const filteredData = [];
  for (let i = 0; i < datas.length; i++) {
    // gender female atau company FSW4 dan age diatas 30 tahun 
    if ((datas[i].gender === "female" || datas[i].company === "FSW4") && datas[i].age > 30) {
      filteredData.push(datas[i]);
    }
  }
  return (filteredData.length < 1) ? errorHandler() : filteredData;
};

const data3Handler = () => {
  const filteredData = [];
  for (let i = 0; i < datas.length; i++) {
    // warna mata biru dan age diantara 35 sampai dengan 40, dan favorit buah apel
    if (datas[i].eyeColor === "blue" && (datas[i].age >= 35 && datas[i].age <= 40) && datas[i].favoriteFruit === "apple") {
      filteredData.push(datas[i]);
    }
  }
  return (filteredData.length < 1) ? errorHandler() : filteredData;
};

const data4Handler = () => {
  const filteredData = [];
  for (let i = 0; i < datas.length; i++) {
    // company Pelangi atau Intel, dan warna mata hijau
    if ((datas[i].company === "Pelangi" || datas[i].company === "Intel") && datas[i].eyeColor === "green") {
      filteredData.push(datas[i]);
    }
  }

  return (filteredData.length < 1) ? errorHandler() : filteredData;
};

const data5Handler = () => {
  const filteredData = [];
  for (let i = 0; i < datas.length; i++) {
    let year = (new Date(datas[i].registered)).getFullYear();

    // registered di bawah tahun 2016 dan masih active(true)
    if (year < 2016 && datas[i].isActive) {
      filteredData.push(datas[i]);
    }
  }
  return (filteredData.length < 1) ? errorHandler() : filteredData;
};

module.exports = { data1Handler, data2Handler, data3Handler, data4Handler, data5Handler };