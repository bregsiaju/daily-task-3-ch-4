// import data
const data = require('./data');

const errorHandler = () => {
  const err = JSON.stringify({
    message: "data tidak ada / tidak ditemukan"
  });
  return err;
};

const data1Handler = () => {
  const filteredData = [];
  for (let i = 0; i < data.length; i++) {
    // age dibawah 30 tahun dan favorit bua pisang
    if (data[i].age < 30 && data[i].favoriteFruit === "banana") {
      filteredData.push(data[i]);
    }
  }
  const filteredLength = outputLength(filteredData);
  return (filteredData.length < 1) ? errorHandler() : JSON.stringify(filteredLength);
};

const data2Handler = () => {
  const filteredData = [];
  for (let i = 0; i < data.length; i++) {
    // gender female atau company FSW4 dan age diatas 30 tahun 
    if ((data[i].gender === "female" || data[i].company === "FSW4") && data[i].age > 30) {
      filteredData.push(data[i]);
    }
  }
  const filteredLength = outputLength(filteredData);
  return (filteredData.length < 1) ? errorHandler() : JSON.stringify(filteredLength);
};

const data3Handler = () => {
  const filteredData = [];
  for (let i = 0; i < data.length; i++) {
    // warna mata biru dan age diantara 35 sampai dengan 40, dan favorit buah apel
    if (data[i].eyeColor === "blue" && (data[i].age >= 35 && data[i].age <= 40) && data[i].favoriteFruit === "apple") {
      filteredData.push(data[i]);
    }
  }
  const filteredLength = outputLength(filteredData);
  return (filteredData.length < 1) ? errorHandler() : JSON.stringify(filteredLength);
};

const data4Handler = () => {
  const filteredData = [];
  for (let i = 0; i < data.length; i++) {
    // company Pelangi atau Intel, dan warna mata hijau
    if ((data[i].company === "Pelangi" || data[i].company === "Intel") && data[i].eyeColor === "green") {
      filteredData.push(data[i]);
    }
  }

  const filteredLength = outputLength(filteredData);
  return (filteredData.length < 1) ? errorHandler() : JSON.stringify(filteredLength);
};

const data5Handler = () => {
  const filteredData = [];
  for (let i = 0; i < data.length; i++) {
    let year = (new Date(data[i].registered)).getFullYear();

    // registered di bawah tahun 2016 dan masih active(true)
    if (year < 2016 && data[i].isActive) {
      filteredData.push(data[i]);
    }
  }
  const filteredLength = outputLength(filteredData);
  return (filteredData.length < 1) ? errorHandler() : JSON.stringify(filteredLength);
};

const outputLength = (filteredData) => {
  let countLength = filteredData.length;
  let allData = filteredData;
  let mergeOutput = {
    "amountOfData": countLength,
    "filteredData": allData
  };
  return mergeOutput;
};

module.exports = { data1Handler, data2Handler, data3Handler, data4Handler, data5Handler };