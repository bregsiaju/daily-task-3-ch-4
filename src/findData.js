const datas = require('./data');

const findData = (search, category) => {
  if (search === undefined || search === "" || category === undefined) {
    return datas;
  }

  const keywords = search === "" ? search : search.toLowerCase();

  const filtered = datas.filter((data) => {
    if (category === "eyeColor") {
      return data.eyeColor === keywords;
    } else if (category === "favoriteFruit") {
      return data.favoriteFruit === keywords;
    } else if (category === "company") {
      return data.company.toLowerCase() === keywords;
    } else if (category === "gender") {
      return data.gender === keywords;
    } else if (category === "age") {
      return data.age.toString() === keywords;
    }
  });

  return filtered;
};

module.exports = findData;